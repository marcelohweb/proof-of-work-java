import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 *import hashlib, struct

	ver = 2
	prev_block = "000000000000000117c80378b8da0e33559b5997f2ad55e2f7d18ec1975b9717"
	mrkl_root = "871714dcbae6c8193a2bb9b2a69fe1c0440399f38d94b3a0f1b447275a29978a"
	time_ = 0x53058b35  # 2014-02-20 04:57:25
	bits = 0x19015f53
	
	nonce = 856192328
	header = (struct.pack("<L", ver) + prev_block.decode('hex')[::-1] +
	          mrkl_root.decode('hex')[::-1] + struct.pack("<LLL", time_, bits, nonce))
	hash = hashlib.sha256(hashlib.sha256(header).digest()).digest()
	print hash[::-1].encode('hex')
 *
 */
public class ProofOfWorkRealTest {

	public static void main(String[] args) throws NoSuchAlgorithmException, IOException {
	
		long version = 2;
		String prev_block = "000000000000000117c80378b8da0e33559b5997f2ad55e2f7d18ec1975b9717";
		String mrkl_root = "871714dcbae6c8193a2bb9b2a69fe1c0440399f38d94b3a0f1b447275a29978a";
		long timestamp = 1392872245;
		long bits = 419520339;
		
		long nonce = 856192328;
		
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
        uint32ToByteStreamLE(version, stream);
        stream.write(reverseBytes(prev_block.getBytes()));
        stream.write(reverseBytes(mrkl_root.getBytes()));
        uint32ToByteStreamLE(timestamp, stream);
        uint32ToByteStreamLE(bits, stream);
        uint32ToByteStreamLE(nonce, stream);
        
        String message = new String( stream.toByteArray(), StandardCharsets.UTF_8 );
		
    	MessageDigest digest = MessageDigest.getInstance("SHA-256");
    	byte[] hash = digest.digest(digest.digest(message.getBytes(StandardCharsets.UTF_8)));
    	String nonceHash = Utils.bytesToHex(reverseBytes(hash));
    	
    	System.out.println(nonceHash);
    	
	}
	
    public static void uint32ToByteStreamLE(long val, OutputStream stream) throws IOException { 
        stream.write((int) (0xFF & (val >> 0))); 
        stream.write((int) (0xFF & (val >> 8))); 
        stream.write((int) (0xFF & (val >> 16))); 
        stream.write((int) (0xFF & (val >> 24))); 
    } 
	
    /**
     * Returns a copy of the given byte array in reverse order. 
     */ 
    public static byte[] reverseBytes(byte[] bytes) { 
        // We could use the XOR trick here but it's easier to understand if we don't. If we find this is really a 
        // performance issue the matter can be revisited. 
        byte[] buf = new byte[bytes.length]; 
        for (int i = 0; i < bytes.length; i++) 
            buf[i] = bytes[bytes.length - 1 - i]; 
        return buf; 
    } 
    
}
