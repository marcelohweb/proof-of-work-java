import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Proof of work algorithm simulator
 *
 */
public class ProofOfWork {

	public static void main(String[] args) throws NoSuchAlgorithmException {

		//Set the difficulty by typing the amount of zeros that you want to the hash to begin
        String dificulty = "0000";
        
        //See more https://en.bitcoin.it/wiki/Block_hashing_algorithm
        
        //Block header
        
        //Used to keep track of protocol upgrades
        long version = 2;
		String prev_block = "000000000000000117c80378b8da0e33559b5997f2ad55e2f7d18ec1975b9717";
		String mrkl_root = "871714dcbae6c8193a2bb9b2a69fe1c0440399f38d94b3a0f1b447275a29978a";
		long timestamp = 1392872245;
		//Target. Changes every 2016 blocks
		long bits = 419520339;
        
        long nonce = 0;
        
        String message = version + new String(Utils.reverseBytes(prev_block.getBytes())) + new String(Utils.reverseBytes(mrkl_root.getBytes())) + timestamp + bits;
        String hashTest = null;
 
        while (true) {
 
        	MessageDigest digest = MessageDigest.getInstance("SHA-256");
        	byte[] hash = digest.digest(digest.digest(message.concat(Long.toString(nonce)).getBytes(StandardCharsets.UTF_8)));
        	hashTest = Utils.bytesToHex(Utils.reverseBytes(hash));
        	System.out.println("Hash: " + hashTest);
        	
            if(hashTest.substring(0, dificulty.length()).equals(dificulty))
            	break;
            nonce++;
 
        }
 
        System.out.println("------------------------");
        System.out.println("Block mined!");
        System.out.println("Nonce: " + nonce);
        System.out.println("Block hash: " + hashTest);

	}
	
}