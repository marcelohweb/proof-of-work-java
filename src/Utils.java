

public class Utils {
	
	/**
	 * Convert a sha256 byte array to hex String
	 * @param hash
	 * @return
	 */
	public static String bytesToHex(byte[] hash) {
	    StringBuffer hexString = new StringBuffer();
	    for (int i = 0; i < hash.length; i++) {
	    String hex = Integer.toHexString(0xff & hash[i]);
	    if(hex.length() == 1) hexString.append('0');
	        hexString.append(hex);
	    }
	    return hexString.toString();
	}

    /**
     * Returns a copy of the given byte array in reverse order. 
     */ 
    public static byte[] reverseBytes(byte[] bytes) { 
        // We could use the XOR trick here but it's easier to understand if we don't. If we find this is really a 
        // performance issue the matter can be revisited. 
        byte[] buf = new byte[bytes.length]; 
        for (int i = 0; i < bytes.length; i++) 
            buf[i] = bytes[bytes.length - 1 - i]; 
        return buf; 
    }
}
